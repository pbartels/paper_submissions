% ---------------------------------------------------------------------
% EG author guidelines plus sample file for EG publication using LaTeX2e input
% D.Fellner, v2.03, Dec 14, 2018


\title[Geometry-Aware Normalizing Flows]%
      {Geometry-Aware Sampling using Normalizing Flows and Geometric Deep Learning}

% for anonymous conference submission please enter your SUBMISSION ID
% instead of the author's name (and leave the affiliation blank) !!
% for final version: please provide your *own* ORCID in the brackets following \orcid; see https://orcid.org/ for more details.
\author[P. Bartels]
{\parbox{\textwidth}{\centering P. Bartels$^{1}$\orcid{0000-0001-5342-4841}}
        \\
% For Computer Graphics Forum: Please use the abbreviation of your first name.
{\parbox{\textwidth}{\centering $^1$KU Leuven, Belgium\\
%         $^2$Graz University of Technology, Institute of Computer Graphics and Knowledge Visualization, Austria
%        $^2$ Another Department to illustrate the use in papers from authors
%             with different affiliations
       }
}
}
% ------------------------------------------------------------------------

% if the Editors-in-Chief have given you the data, you may uncomment
% the following five lines and insert it here
%
% \volume{36}   % the volume in which the issue will be published;
% \issue{1}     % the issue number of the publication
% \pStartPage{1}      % set starting page


\graphicspath{{src/img/}}

%-------------------------------------------------------------------------
\begin{document}

% uncomment for using teaser
% \teaser{
%  \includegraphics[width=\linewidth]{eg_new}
%  \centering
%   \caption{New EG Logo}
% \label{fig:teaser}
%}

\maketitle
%-------------------------------------------------------------------------
% TODO: should I mention locality in the abstract?
\begin{abstract}
   Modern physically based rendering is based on Monte Carlo integration techniques
   to approximate the rendering equation. The convergence rate of these techniques
   can be increased by sampling according to a distribution that matches the integrand.
   The integrand defined in the rendering equation is highly geometry-dependent.
   In this paper, we propose a deep-learning based method to create sampling distributions that model this dependency on the scene geometry.
   Our framework is based on Normalizing Flows, a recent generative modeling technique with advantages that
   make it well-suited for integration into modern renderers.
   By conditioning these normalizing flows on features computed with Geometric Deep Learning,
   we allow our network to learn geometry-aware sampling distributions.
   Implicitly, the network learns a representation of the geometry that is task-dependent, which we showcase through training networks on varying problems.
   Furthermore, our network structure is set up so that it allows us to train offline,
   on smaller, less complex scenes. More complex geometry can then be processed before the actual rendering, saving the computed features as per-vertex textures and
   limiting the need for network inference during rendering.\\

%-------------------------------------------------------------------------
%  ACM CCS 1998
%\begin{classification} % according to https://www.acm.org/publications/computing-classification-system/1998
%\CCScat{Computer Graphics}{I.3.3}{Picture/Image Generation}{Line and curve generation}
%\end{classification}
%-------------------------------------------------------------------------
%  ACM CCS 2012 (see https://www.acm.org/publications/class-2012)
%The tool at \url{http://dl.acm.org/ccs.cfm} can be used to generate
% CCS codes.
%Example:
\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10010147.10010371.10010372.10010374</concept_id>
<concept_desc>Computing methodologies~Ray tracing</concept_desc>
<concept_significance>500</concept_significance>
</concept>
<concept>
<concept_id>10010147.10010371.10010372.10010377</concept_id>
<concept_desc>Computing methodologies~Visibility</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Computing methodologies~Ray tracing}
\ccsdesc[500]{Computing methodologies~Visibility}


\printccsdesc
\end{abstract}
%-------------------------------------------------------------------------
\section{Introduction}

Many application domains, such as visual effects, gaming and architecture,
require the ability to compute photo-realistic images of digital scenes.
Current state-of-the-art algorithms that create such images are based on approximating the integral
in the rendering equation through Monte Carlo sampling. These techniques estimate an integral by
averaging the value of the integrand at randomly sampled locations in the integration domain.\\

Various techniques exist to decrease the variance of these techniques, with importance sampling being a notable one.
This entails choosing the random samples according to a distribution that closely resembles the integrand.
Due to the nature of the integrand in the rendering equation, finding such distributions is non-trivial:
it is recursive, combines multiple complex terms and is, in general, not continuous.
Importance sampling has been a long-standing research problem in the rendering community and various techniques have been proposed that
sample according to one or multiple terms in the rendering equation (TODO: references).
An important requirement for these techniques is speed: sampling happens billions of times for high-quality images. (TODO: replace 'billions')
Moreover, modern renderers combine multiple sampling strategies through the use of Multiple Importance Sampling (TODO: REF).
The use of Multiple Importance Sampling adds the additional requirement
that the probability of a sample should be computable, even when the sample was generated through another strategy.
Because of the above requirements, Normalizing Flows have recently proven to be excellent generative models.\\

Unfortunately, one of the main difficulties in sampling the rendering equation is the
fact that the integral domain is determined by the geometry of the scene to be rendered.
This means that optimal importance sampling would include knowledge of the geometry present in the scene,
something that has been difficult to accomplish. One current attempt is path guiding (TODO: REF + related work, NIS), where
rendering is started without knowledge of the geometry, and is gradually built up. In this technique, however, initial samples
have to be discarded, and learning and inference has to happen during rendering, slowing the image generation process down considerably.
Another branch of research ideas consists of specialized sampling techniques for specific tasks, where geometry is represented
through hand-crafted features. (TODO: REF subsurface paper)\\

In this paper we bridge the gap between these two through Geometric Deep Learning (GDL),
a branch of deep learning concerned with learning features on non-euclidean domains. GDL
allows us to automatically learn features suitable for a specific task, while also doing so
offline on simple objects. The learned geometric convolution kernels can then be applied on
the geometry to be rendered before the actual rendering, trading off severely increased
render-times for increased memory usage. \\
In short, our contributions are:
\begin{itemize}
\item We introduce Geometric Deep Learning techniques to learn implicit task-dependent geometry representations that can be computed pre-rendering.
\item We condition Normalizing Flows on these representations, to create highly-expressive geometry-aware sampling distributions.
\item We show several use cases of these distributions, proving the framework's flexibility as well as providing guidance for adoption.
\end{itemize}

%-------------------------------------------------------------------------
\section{Related Work}

\subsection{Importance Sampling}
subsurface paper. Path guiding. MIS.


\subsection{Normalizing Flows}
Normalizing Flows (Dinh et al., review paper)
NIS (Marios)

\subsection{Geometric Deep Learning}
overview paper.
Different representations.
GMM. SplineCNN.

\subsection{Other Deep Learning techniques}
Hermosilla. Cloud paper.


\section{Core Idea}

This section explains our framework on conceptual level. For this, we first introduce normalizing flows in more detail, then we explain geometric deep learning. The section concludes with a detailed overview of how our framework combines the two, what advantages the combination gives, and what design questions are present when using the framework in practice.

\subsection{Normalizing flows}

A so-called normalizing flow is fairly novel machine learning technique from the field of generative modeling (REF). They allow efficient learning and sampling of arbitrary, high-frequency joint probability distribution functions (pdf, plural pdfs) over multi-dimensional continuous random variables.\\

Normalizing flows operate by transforming between the sought-after random variable and a second, easy to evaluate random variable of the same dimensionality. Let $p_t(x)$ be the target joint probability distribution function we want to model, defined over the real-valued, d-dimensional vector $x$. This pdf is not given analytically, but implied by the data samples $X$.  We choose a second pdf $p_u(u)$ that is easy to evalute and sample from, defined over a real-valued d-dimensional vector $u$. In line with (REF), we call this the base distribution. In addition, we choose a transform $T$ that can convert $x$ to $u$ and vice versa:\\
\begin{equation}
\begin{split}
& x = T(u)\\
& u = T^{-1}(x)
\end{split}
\end{equation}
It is key for normalizing flows that this transformation $T$ is invertible, as already implied in the notation above, as well as differentiable in both directions. If the transformation is chosen as such, $p(x)$ is well-defined throught the change-of-variables formula:
\begin{equation}
p(x) = p(u)\ | det(J_T(u))|^{-1}
\end{equation}
where $J_T(u)$ is the jacobian of $T(u)$, which is a $dxd$ matrix, and $det$ takes the determinant of this matrix. This equation can be used to sample $x$ through $u$, i.e. for inference on the model. We can also write $p_t(x)$ in terms of $x$ itself, using $T^{-1}$:
\begin{equation}
p_t(x) = p_u(T^{-1}(x))\ |det(J_{T^{-1}}(x))|
\end{equation}
which will be used for training the model.\\

In order to use the above idea to perform generative modeling, the transformation T is parameterized. Some discrepancy measure between the normalizing flow model pdf p(x) (equation REF) and the empirical distribution implied by the data samples X is then minimized with respect to the transformation parameters, e.g. $T(u | \phi)$. In practice, this usually means using an iterative stochastic gradient-descent method to minimize the KL divergence between those two distributions:
\begin{equation}
TODO: FORMULA KL Divergence
\end{equation}

The cornerstone of normalizing flows is an appropriate definition of the transformation $T$. The choice made for this will determine the expressive power of the trained model, implying the need for a powerful transformation. On the other hand, complex transformations might be slow to evaluate, either during training or inference. Luckily, the requirement on the transformations to be invertible and differentiable also implies that they will composable, as a succession of two such transformations $T_1$, $T_2$ gives a new invertible, differentiable transformation $T_{new}$:
\begin{equation}
\begin{split}
x &= T_{new}(u) = T_2 \circ T_1(u)\\
u &= T_{new}^{-1}(x) = (T_2 \circ T_1)^{-1}(x) = T_1^{-1} \circ T_2^{-1}(x)\\
det(J_{T_{new}}(u)) &= det(J_{T_2}(T_1(u)))\cdot det(J_{T_1}(u))
\end{split}
\end{equation}
We can thus build a powerful, complex transformation by chaining together simpler ones with desirable properties.\\

TODO: describe useful examples of simple transformations: original ones, coupling layers, NIS layers.

\subsection{Geometric Deep Learning}

We propose using Geometric Deep Learning to compute these additional features from Geometry, allowing us to learn probability distributions that have knowledge of the surrounding geometry.
We show that this is useful for several use cases in rendering and global illumination computations.

\section{Proof of Concept}

We put our idea to the test and show its viability on a simple example: sampling points on a mesh.

\section{Experiments}

\subsection{Forward path tracing through Caustics}
\subsection{Position Sampling for Subsurface Scattering}
\subsection{Visibility-aware BRDF sampling}

\section{Conclusion}

here we conclude our work and indicate avenues for future work.

\end{document}
