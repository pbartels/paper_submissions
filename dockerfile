FROM ubuntu:18.04

RUN apt-get clean &&\
    apt-get update &&\
    apt-get autoclean -y &&\
    apt-get autoremove -y &&\
    apt-get update &&\
    apt-get install -f -y apt-utils

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y texlive-base texlive texlive-science latexmk texlive-latex-extra
RUN apt-get install -y make
